#!/usr/bin/env guile \
-e main -s
!#

(use-modules
 (ice-9 rdelim)
 (srfi srfi-1)
 (ice-9 popen)
 (ice-9 textual-ports)
 (ice-9 receive)
 (ice-9 regex)
 (ice-9 pretty-print)
 (srfi srfi-13))

(define keyframe-ref (make-hash-table))
(define encoder-codec (make-hash-table))

(define +csv-header+ '(scene start end infile outfile
                             vcodec acodec input-opts output-opts))

(define +ffmpeg-time-regexp+ (make-regexp "t:[0-9]+.[0-9]+"))

;; ---
(define (interlace-list->alist list1 list2)
  (reverse! (fold (lambda (x y prev) (cons (cons x y) prev))
                  '() list1 list2)))

(define (set-difference list1 list2)
  (let ((res '()))
    (for-each
     (lambda (x) (unless (member x list2)
                   (set! res (cons x res))))
     list1)
    res))

(define (collect-until-when pred when? src proc)
  (let* ((result (src))
         (final-result '()))
    (while (not (pred result))
      (when (when? result)
        (set! final-result (cons (proc result) final-result)))
      (set! result (src)))
    (reverse! final-result)))
;; ---

(define (ffmpeg-line->seconds line)
  (let* ((line-match (regexp-exec +ffmpeg-time-regexp+ (string-trim-both line)))
         (start (match:start line-match))
         (end (match:end line-match))
         (str (substring line (+ 2 start) end)))
    str))


(define (get-keyframe-list file)
  (let* ((hash-key (string->symbol file))
         (keyframe-list (hash-ref keyframe-ref hash-key)))
    (or keyframe-list
        (let ((pipe (open-input-pipe
                     (string-append "AV_LOG_FORCE_NOCOLOR=1 ffmpeg -hide_banner -i '"
                                    file
                                    "' -vf select='eq(pict_type\\,PICT_TYPE_I)' -f null NUL -loglevel debug 2>&1"))))
          (hash-set! keyframe-ref hash-key
                     (collect-until-when eof-object?
                                         (lambda (line)
                                           (and (string? line)
                                                (string-contains line "select_out:0")))
                                         (lambda ()
                                           (read-line pipe))
                                         (lambda (line)
                                           (ffmpeg-line->seconds line))))
          (close-pipe pipe)
          (hash-ref keyframe-ref hash-key)))))

(define* (find-keyframe-before-after file start end #:optional round-down)
  (let ((closest-start "")
        (closest-end "")
        (previous-end end)
        (nstart (string->number start))
        (nend (string->number end)))

    (let loop ((keyframes (get-keyframe-list file)))
      (let* ((seconds (car keyframes))
             (nseconds (string->number seconds)))
        (if round-down
            (begin
              (when (and (string-null? closest-start) (> nseconds nstart))
                (set! closest-start seconds))
              (when (> nseconds nend)
                (set! closest-end previous-end))
              (set! previous-end seconds))
            (begin
              (when (< nseconds nstart)
                (set! closest-start seconds))
              (when (> nseconds nend)
                (set! closest-end seconds)))))

      (when (string-null? closest-end)
        (loop (cdr keyframes))))

    (values closest-start closest-end)))

(define (find-video-codec file)
  (let* ((pipe (open-input-pipe
                (string-append "mediainfo  '"
                               file "' --Inform='Video;%Format%'")))
         (line (read-line pipe)))
    (close-pipe pipe)
    (string-trim-both line)))

(define (ffmpeg-avaliable-codecs format)
  (let* ((hash-key (string->symbol format))
         (encoders (hash-ref encoder-codec hash-key)))
    (or encoders
        (let ((pipe (open-input-pipe
                     (string-append "ffmpeg -hide_banner -h encoder='"
                                    format
                                    "'"))))
          (hash-set! encoder-codec hash-key
                     (collect-until-when eof-object?
                                         (lambda (line)
                                           (and (string? line)
                                                (string-prefix? "Encoder" line)))
                                         (lambda ()
                                           (read-line pipe))
                                         (lambda (line)
                                           (second (string-split line #\space)))))
          (close-pipe pipe)
          (hash-ref encoder-codec hash-key)))))

(define (ffmpeg-find-codec file)
  (let ((codec (find-video-codec file)))
    (first (ffmpeg-avaliable-codecs (string-downcase codec)))))

(define (format-ffmpeg-base-command stream input-options output-options infile outfile)
  (format stream "ffmpeg -hide_banner ~a -i '~a' ~a ~a~%"
          input-options infile output-options outfile))

(define (format-ffmpeg-command-fast stream start end infile outfile)
  (format stream
          "ffmpeg -ss '~a' -to '~a' -i '~a' -vcodec copy -acodec copy -map 0 '~a'"
          start end infile outfile))

(define (format-ffmpeg-command-accurate stream start end infile outfile)
  (format stream
          "ffmpeg -i '~a' -ss '~a' -to '~a' -vcodec copy -acodec copy -map 0 '~a'"
          infile start end outfile))

(define (format-ffmpeg-command-efficient stream prestart start end infile outfile)
  (format stream
          "ffmpeg -ss '~a' -i '~a' -ss '~a' -to '~a' -vcodec copy -acodec copy -map 0 '~a'"
          prestart infile start end outfile))

(define (format-ffmpeg-command-re-encode-video stream prestart start end infile vcodec outfile)
  (format-ffmpeg-base-command
   stream
   (or (and prestart (format #f "-ss '~a'" prestart)) "")
   (format #f "-ss '~a' -to '~a' -vcodec '~a' -acodec copy -map 0"
           start end vcodec)
   infile outfile))


(define (create-ffmpeg-joinfile-raw input)
  "takes list of alists of key:value ffmpeg options
https://ffmpeg.org/ffmpeg-formats.html#Syntax"
  (with-output-to-string
    (lambda ()
      (for-each (lambda (file-line)
                  (for-each (lambda (option)
                              (format #t "~a ~a~%"
                                      (car option)
                                      (cdr option)))
                            file-line))
                input))))

(define (create-ffmpeg-joinfile input)
  "Takes list of lists (file inpoint outpoint) and creates ffmpeg concat text"
  (let ((input-map '(file inpoint outpoint)))
    (create-ffmpeg-joinfile-raw
     (map-in-order (lambda (x)
                     (interlace-list->alist input-map x))
                   input))))

(define (generate-tempfile-command stream context suffix)
  (let ((tempvar (format #f "~a_tmp"
                         (string-filter char-set:letter context))))
    (format stream "~a=$(mktemp -u --tmpdir 'rs_XXXXXXXX.~a')~%" tempvar suffix)
    tempvar))

(define (generate-delete-tempfile-command stream tmpfile)
  (format stream "rm \"$~a\"~%" tmpfile))

(define (format-ffmpeg-join-command input input-opts output-opts outfile)
  (let ((tempvar (format #f "~a_merge_tmp"
                         (string-filter char-set:letter (basename outfile)))))
    (string-append tempvar "=$(mktemp -u --tmpdir 'rs_merge_XXXXXXXX.txt')\n"
                   "cat << RUNNING_SCISSORS_EOF > \"$" tempvar "\"\n"
                   (create-ffmpeg-joinfile input)
                   "RUNNING_SCISSORS_EOF\n\n"
                   "ffmpeg -hide_banner -f concat -safe 0 " input-opts  " -i \"$" tempvar "\" "
                   output-opts " '" outfile "'\n\n"
                   "rm \"$" tempvar "\"\n")))

(define (shell-var var)
  (string-append "$" var))

(define (shell-unescape-var var)
  (string-append "\"$" var "\""))

(define (shell-escape var)
  (string-append "'" var "'"))

(define (generate-ffmpeg-copy-cut-command infile start end outfile)
  (with-output-to-string
    (lambda ()
      (receive (first-keyframe last-keyframe)
          (find-keyframe-before-after infile start end #t)
        (let* ((file-ext (last (string-split (basename outfile) #\.)))
	       ;; codec autodetection needs manual override option
	       (codec (ffmpeg-find-codec infile))
               (start-tmp (generate-tempfile-command
                           #t (format #f "~a_start" (basename outfile))
                           file-ext))
               (end-tmp (generate-tempfile-command
                         #t (format #f "~a_end" (basename outfile))
                         file-ext)))

          (newline)
          (format-ffmpeg-command-re-encode-video
           #t #f start first-keyframe infile codec (shell-unescape-var start-tmp))
          (format-ffmpeg-command-re-encode-video
           #t #f last-keyframe end infile codec (shell-unescape-var end-tmp))
          (newline)
          (display
           (format-ffmpeg-join-command
            `((,(shell-var start-tmp))
              (,(shell-escape infile) ,first-keyframe ,last-keyframe)
              (,(shell-var end-tmp)))
            ""
            "-acodec copy -vcodec copy"
            outfile))


          (generate-delete-tempfile-command #t start-tmp)
          (generate-delete-tempfile-command #t end-tmp)
          (display "#----------------------------------------")
          (newline)
          (newline))))))

(define (build-output-filename outfile scene)
  ;; bit hacky this
  (let ((filename-index (string-rindex outfile #\/)))
    (string-append (if filename-index (substring outfile 0 (+ filename-index 1)) "")
                   (format #f "~a_" scene)
                   (basename outfile))))

(define (generate-ffmpeg-command input)
  ;; there has to be a nicer way of destructuring that alist
  (let* ((scene (assoc-ref input 'scene))
         (start (assoc-ref input 'start))
         (end (assoc-ref input 'end))
         (infile (assoc-ref input 'infile))
         (outfile (build-output-filename  (assoc-ref input 'outfile) scene)))
    (unless (any string-null? (list start end infile outfile))
      (begin
        (display (generate-ffmpeg-copy-cut-command infile start end outfile))
        ;; (receive (actual-start actual-end)
        ;;     (find-keyframe-before-after infile start end)
        ;;   (format-ffmpeg-command-accurate #t actual-start actual-end infile outfile)
        ;;   (newline))
        ))))

(define (csv-line->alist line)
  "Takes raw CSV line and maps it to +csv-header+"
  (interlace-list->alist +csv-header+ (map string-trim-both
                                           (string-split line #\,))))

(define (parse-csv file)
  (let ((line (read-line file)))
    (unless (eof-object? line)
      (generate-ffmpeg-command (csv-line->alist line))
      (parse-csv file))))

(define (main args)
  (when (< (length args) 2)
    (format #t "usage: ~a <project.csv>~%" (first args))
    (quit -1))
  (call-with-input-file (second args) parse-csv)
  ;; (get-keyframe-times (second args))
  )
