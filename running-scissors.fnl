(local mp (require "mp"))
(local msg (require "mp.msg"))
(local options (require "mp.options"))
(local utils (require "mp.utils"))
(local script-name "rs")
(local scenes [])
(var current-scene 0)

(local o {
          "time-seconds" true
          "default-output-file" "snip.mkv"
          "project-file" "snip.csv"})
(options.read_options o)

(fn ensure-scene [scene]
    (when (not (. scenes scene))
      (tset scenes scene [])))

(fn set-scene-property [scene prop val]
    "Sets scene property"
    (msg.info "Set scene" scene prop "to" val)
    (ensure-scene scene)
    (tset (. scenes scene) prop val))

(fn get-scene-property [scene prop]
    "Sets scene property"
    (. (. scenes scene) prop))

(fn set-scene-start [scene start]
    "Sets scene start time"
    (set-scene-property scene "start" start))

(fn set-scene-end [scene end]
    "Sets scene end time"
    (set-scene-property scene "end" end))

(fn set-scene-filename [scene filename]
    "Sets scene filename"
    (set-scene-property scene "filename" filename))

(fn format-scene-properties [scene]
    (if (. scenes scene)
        (string.format "%s: %s-%s -> %s"
                       scene
                       (get-scene-property scene "start")
                       (get-scene-property scene "end")
                       (get-scene-property scene "filename"))
        "invalid scene"))

(fn display-scene-properties [scene]
    (let [message (format-scene-properties scene)]
      (msg.info message)
      (mp.osd_message message 2)))

(fn display-current-scene-properties []
    (display-scene-properties current-scene))

(fn next-scene []
    (set current-scene (+ current-scene 1))
    (ensure-scene current-scene)
    (when (not (get-scene-property current-scene "filename"))
      (set-scene-property current-scene "filename"
                          (get-scene-property (- current-scene 1) "filename")))
    (display-current-scene-properties))

(fn previous-scene []
    (set current-scene (- current-scene 1))
    (ensure-scene current-scene)
    (display-current-scene-properties))

(fn get-current-time []
    (let [seconds (mp.get_property "time-pos")]
      (if (. o "time-seconds")
          seconds
          (mp.format_time seconds "%H:%M:%S.%T"))))

(fn set-start-time []
    (set-scene-property current-scene "start" (get-current-time))
    (display-scene-properties current-scene))

(fn set-end-time []
    (set-scene-property current-scene "end" (get-current-time))
    (display-scene-properties current-scene))

(fn file-exists [file]
    (let [fin (io.open file)]
      (when fin
          (io.close fin)
          true)))

(fn load-file [file]
    (with-open [fin (io.open file)]
      (each [line (fin:lines)]
            (let [vals []]
              (each  [value (string.gmatch line "([^,]+)")]
                     (table.insert vals value))
              (let [scene (tonumber (. vals 1))]
                (set-scene-start scene (. vals 2))
                (set-scene-end scene (. vals 3))
                (set-scene-filename scene (. vals 5)))))))

(fn get-scene-property-str [scene prop]
    (let [val (get-scene-property scene prop)]
      (if val val " ")))

(fn write-file [file]
    (with-open [fout (io.open file :w)]
      (for [scene 0 (length scenes)]
           (let [str (string.format "%s,%s,%s,%s,%s\n"
                                    scene
                                    (get-scene-property-str scene "start")
                                    (get-scene-property-str scene "end")
                                    (mp.get_property "path")
                                    (get-scene-property-str scene "filename"))]
             (fout:write str)))))

(fn save []
    (let [filename (. o "project-file")
          message (string.format "wrote to %s" filename)]
      (write-file filename)
      (msg.info message)
      (mp.osd_message message)))

(fn load []
    (let [filename (. o "project-file")
          message (string.format "loaded from %s" filename)]
      (when (file-exists filename)
        (load-file filename)
        (msg.info message)
        (mp.osd_message message))))



(fn seek [time]
    (if time
        (mp.set_property "time-pos" time)
        (msg.warn "attempted to seek to a emtpy time!")))

(fn handle-file-save [stdout]
    (msg.info stdout))

(fn ask-save-file-name [default-filename]
    (string.gsub (. (utils.subprocess
                     {"args" ["zenity" "--file-selection" "--save"
                                       "--filename" default-filename]}) "stdout")
                 "^%s*(.-)%s*$" "%1"))

(fn set-output-filename []
    (set-scene-property current-scene "filename"
                        (ask-save-file-name
                         (get-scene-property current-scene "filename"))))

(fn test []

    (let [ message (string.format "Hello")]
      (msg.info message)
      (mp.osd_message message)))

(fn goto-start-time []
    (seek (get-scene-property current-scene "start")))

(fn goto-end-time []
    (seek (get-scene-property current-scene "end")))

(fn init []
    (set-scene-property 0 "filename" (. o "default-output-file"))
    (load))

(mp.add_key_binding "N" "next-scene" next-scene)
(mp.add_key_binding "P" "previous-scene" previous-scene)
(mp.add_key_binding "S" "set-start-time" set-start-time)
(mp.add_key_binding "s" "goto-start-time" goto-start-time)
(mp.add_key_binding "e" "goto-end-time" goto-end-time)
(mp.add_key_binding "E" "set-end-time" set-end-time)
(mp.add_key_binding "r" "show-scene-properties" display-current-scene-properties)

(mp.add_key_binding "W" "save" save)
(mp.add_key_binding "f" "set-filename" set-output-filename)

(mp.add_key_binding "t" "test" test)

(mp.register_event "file-loaded" init)
